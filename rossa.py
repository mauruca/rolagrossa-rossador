from bs4 import BeautifulSoup
import requests

from requests.exceptions import HTTPError

for url in ['https://www.valor.com.br/carteira-valor-iframe/']:
    try:
        response = requests.get(url)

        soup = BeautifulSoup(response.text,'html.parser')
        
        print(soup.find("div","container-corretoras").get_text("|", strip=True))

    except HTTPError as http_err:
        print(f'HTTP error occurred: {http_err}')  # Python 3.6
    except Exception as err:
        print(f'Other error occurred: {err}')  # Python 3.6
    else:
        print('Success!')



